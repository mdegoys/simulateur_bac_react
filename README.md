# README

## Project description

This project is a "grade simulator", made for highschool students to compute their potential final grade and merit at the French national exam "Baccalauréat". It can be visited at https://www.simulateur-bac.fr

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn cy:test`

Launches the end-to-end tests.

## Tools used

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).\
Style is handled by [Tailwind CSS](https://tailwindcss.com/).\
End 2 end tests have been written using [Cypress](https://www.cypress.io/).\
CI and page hosting are powered by [Gitlab](https://gitlab.com).

## Feedback

Any feedback is appreciated. You can:
- create a merge request (in English please :))
- and/or contact me directly (in English or French :)). Contact details are available on my blog footer.
