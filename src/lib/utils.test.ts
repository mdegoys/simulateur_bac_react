import '@testing-library/jest-dom';
import { makeInputGradeOrEmptyString, overallAverage, examResultYear, isThemeDark, fileName } from './utils'
import Constants from '../data/constants';

describe('makeInputGradeOrEmptyString()', () => {
  it("doesn't touch an number between 0 and 20", () => {
    expect(makeInputGradeOrEmptyString(10)).toBe(10);
  });

  it('turns string into number', () => {
    expect(makeInputGradeOrEmptyString('0.01')).toBe(0.01);
  });

  it('replaces commas with dots',  () => {
    expect(makeInputGradeOrEmptyString('0,01')).toBe(0.01);
  });

  it('returns null if the input is not a number', () => {
    expect(makeInputGradeOrEmptyString('O')).toBe("");
  });

  it ('returns null if the input is a number exceeding 20', () => {
    expect(makeInputGradeOrEmptyString(21)).toBe("");
    expect(makeInputGradeOrEmptyString(20.01)).toBe("");
    expect(makeInputGradeOrEmptyString(20)).toBe(20);
    expect(makeInputGradeOrEmptyString(0)).toBe(0);
  });
});

describe('overallAverage()', () => {
  it('returns the grade if one grade has been entered', () => {
    expect(overallAverage({ gradesTotal: 10, coefficientsTotal: 1 })).toBe(10);
  });

  it("returns '-' if no grades have been entered", () => {
    expect(overallAverage({ gradesTotal: 0, coefficientsTotal: 0 })).toBe('-');
  });

  it('returns up to two decimals if average is not round', () => {
    expect(overallAverage({ gradesTotal: 10, coefficientsTotal: 3 })).toBe(3.33);
  });

  it('returns no decimals if first two are 00', () => {
    expect(overallAverage({ gradesTotal: 36.66666666666667, coefficientsTotal: 3.3333333333333335 })).toBe(11);
  });
});

describe('examResultYear()', () => {
  it('returns the following year if we are past september', () => {
    let date = new Date('September 19, 1975');
    expect(examResultYear(date)).toBe('1976');
  });

  it('returns the current year if we are past January', () => {
    let date = new Date('January 5, 2056');
    expect(examResultYear(date)).toBe('2056');
  });
});

describe('isThemeDark()', () => {
  it('returns true if the theme is "dark"', () => {
    expect(isThemeDark(Constants.DARK_THEME)).toBe(true);
  });

  it('returns false if the theme is "light"', () => {
    expect(isThemeDark(Constants.LIGHT_THEME)).toBe(false);
  });
 });

describe('fileName()', () => {
  it('returns the right file name when name is Technologique', () => {
    let trackName = 'Technologique';
    expect(fileName(trackName)).toBe('2019_depliant_1re_technologique_reussirbac.pdf');
  })
  it('returns the right file name when name is Générale', () => {
    let trackName = 'Générale';
    expect(fileName(trackName)).toBe('2019_depliant_1re_generale_reussirbac.pdf');
  })
  it('returns the "générale" file name when name is undefined', () => {
    let trackName;
    expect(fileName(trackName)).toBe('2019_depliant_1re_generale_reussirbac.pdf');
  })

});
