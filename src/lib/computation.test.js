import '@testing-library/jest-dom';
import { maximumGradesNumber, gradesNumber, coefficientsTotal, gradesTotal, isMeritForAverage } from './computation'
import Constants from '../data/constants';
import { merits } from '../data/merits';

describe('isMeritForAverage()', () => {
  it('returns true when given the merit minimum grade', () => {
    merits.forEach(merit => {
      expect(isMeritForAverage(merit.minGrade)(merit)).toBe(true);
    })
  });

  it('returns true when given an average between min and max grade', () => {
    merits.forEach(merit => {
      let gradeBetweenMinAndMax = (merit.minGrade+merit.maxGrade)/2;
      expect(isMeritForAverage(gradeBetweenMinAndMax)(merit)).toBe(true);
    })
  });

  it('returns false when given the merit maxium grade', () => {
    merits.forEach(merit => {
      expect(isMeritForAverage(merit.maxGrade)(merit)).toBe(false);
    })
  })
})

describe('maximumGradesNumber()', () => {
  it('returns the number of possible tests results', () => {
    let trackTestsValues = {
    "trackName": {
      "complete": "Voies générales",
      "shortened": "Générale"
    },
    "testsTypes": [
      {
        "testsTypeName": "Three possible results in total",
        "tests": [
          { "testName": "One possible result",
            "testResults": [
              {
                "grade": 0,
                "coefficient": 1
              }
            ],
          },
          { "testName": "Two possible results",
            "testResults": [
              {
                "grade": '8.23',
                "coefficient": 0.5
              },
              {
                "grade": '',
                "coefficient": 0.5
              }
            ],
          },
        ]
      }
    ]
  }

  expect(maximumGradesNumber(trackTestsValues)).toBe(3);
  });

  it("doesn't count the 'na' tests", () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "Bulletins scolaires",
          "tests": [
            { "testName": "Moyenne générale",
              "testResults": [
                { "grade": '',
                  "coefficient": 1
                },
                {
                  "grade": Constants.NOT_APPLICABLE,
                  "coefficient": 0
                }
              ]
            },
          ]
        }
      ]
    }

  expect(maximumGradesNumber(trackTestsValues)).toBe(1);
  });

  it("counts only the tests with optional set to true and at least one grade entered", () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "Bulletins scolaires",
          "tests": [
            { "testName": "counts as optional false",
              "testResults": [
                {
                  "grade": '',
                  "coefficient": 1
                },
              ],
              "optional": false
            },
            { "testName": "counts as optional not set",
              "testResults": [
                {
                  "grade": '',
                  "coefficient": 1
                },
              ],
            },
            { "testName": "won't count as optional set to true and no grade entered",
              "testResults": [
                {
                  "grade": '',
                  "coefficient": 1
                },
              ],
              "optional": true
            },
            { "testName": "counts as optional set to true and one grade entered",
              "testResults": [
                {
                  "grade": '',
                  "coefficient": 1
                },
                {
                  "grade": '2',
                  "coefficient": 1
                },
              ],
              "optional": true
            },
          ]
        }
      ]
    }

  expect(maximumGradesNumber(trackTestsValues)).toBe(4);
  });
});

describe('gradesNumber()', () => {
  it('returns the number of tests results', () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "Two tests in total",
          "tests": [
            { "testName": "One result",
              "testResults": [
                {
                  "grade": '0',
                  "coefficient": 1
                },
                {
                  "grade": Constants.NOT_APPLICABLE,
                  "coefficient": 0
                }
              ]
            },
            { "testName": "One other result",
              "testResults": [
                {
                  "grade": '12.12',
                  "coefficient": 0.5
                },
                {
                  "grade": '',
                  "coefficient": 0.5
                }
              ]
            },
          ]
        }
      ]
    }

  expect(gradesNumber(trackTestsValues)).toBe(2);
  });

  it("doesn't take into account tests with optional set to true and grade entered", () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "Two tests in total",
          "tests": [
            { "testName": "One mandatory exam",
              "testResults": [
                {
                  "grade": '0',
                  "coefficient": 1
                },
                {
                  "grade": Constants.NOT_APPLICABLE,
                  "coefficient": 0
                }
              ]
            },
            { "testName": "One other optional exam but taken",
              "testResults": [
                {
                  "grade": '12.12',
                  "coefficient": 0.5
                },
                {
                  "grade": '',
                  "coefficient": 0.5
                }
              ],
              "optional": true,
            },
            { "testName": "One other optional exam but not taken",
              "testResults": [
                {
                  "grade": '',
                  "coefficient": 0.5
                },
                {
                  "grade": '',
                  "coefficient": 0.5
                }
              ],
              "optional": true
            },
          ]
        }
      ]
    }

  expect(gradesNumber(trackTestsValues)).toBe(2);
  });
})


describe('coefficientsTotal()', () => {
  it('returns the sum of coefficient for which result have been entered', () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "some tests",
          "tests": [
            { "testName": "Counts as 1",
              "testCoefficient": 1,
              "testResults": [
                {
                  "grade": '0',
                  "coefficient": 1
                }
              ]
            },
            { "testName": "Counts as 2",
              "testResults": [
                {
                  "grade": '12.12',
                  "coefficient": 2
                },
                {
                  "grade": Constants.NOT_APPLICABLE,
                  "coefficient": 0
                }
              ]
            },
          ]
        }
      ]
    }

  expect(coefficientsTotal(trackTestsValues)).toBe(3);
  });

  it("doesn't take into account coefficientsTotal when optional is set to true and no grade entered", () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "some tests",
          "tests": [
            {
              "testName": "counts as 1 as optionTaken not defined",
              "testResults": [
                {
                  "grade": '0',
                  "coefficient": 1
                }
              ]
            },
            {
              "testName": "counts as 1 as optional set to true and 1 grade entered",
              "testResults": [
                {
                  "grade": '12.12',
                  "coefficient": 2
                },
                {
                  "grade": '',
                  "coefficient": 1
                }
              ],
              "optional": true
            },
            {
              "testName": "doesn't count as optional set to true and no grade entered",
              "testResults": [
                {
                  "grade": '',
                  "coefficient": 1
                },
                {
                  "grade": '',
                  "coefficient": 1
                }
              ],
              "optionTaken": true
            },
          ]
        }
      ]
    }

  expect(coefficientsTotal(trackTestsValues)).toBe(3);
  });
})

describe('gradesTotal()', () => {
  it('returns the sum of grades entered, multiplying each by its coefficient', () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "some tests",
          "tests": [
            { "testName": "Counts as 5",
              "testResults": [
                {
                  "grade": '5',
                  "coefficient": 1
                },
              ],
            },
            { "testName": "Counts as 20",
              "testResults": [
                {
                  "grade": '10',
                  "coefficient": 2
                },
                {
                  "grade": Constants.NOT_APPLICABLE,
                  "coefficient": 0
                },
              ],
            },
          ]
        }
      ]
    }

  expect(gradesTotal(trackTestsValues)).toBe(25);
  });

  it("doesn't take into account grade when optional is set to false or when no grade entered", () => {
    let trackTestsValues = {
      "trackName": {
        "complete": "Voies générales",
        "shortened": "Générale"
      },
      "testsTypes": [
        {
          "testsTypeName": "some tests",
          "tests": [
            {
              "testName": "Counts as optional not defined",
              "testResults": [
                {
                  "grade": '5',
                  "coefficient": 1
                },
              ],
            },
            {
              "testName": "Counts as optional set to true and 1 grade entered",
              "testResults": [
                {
                  "grade": '10',
                  "coefficient": 3
                },
              ],
              "optional": true
            },
            {
              "testName": "doesn't count as optional set to true and no grade entered",
              "testResults": [
                {
                  "grade": '',
                  "coefficient": 1
                },
              ],
              "optional": true
            },
            {
              "testName": "counts as optional set to false",
              "testResults": [
                {
                  "grade": '20',
                  "coefficient": 1
                },
              ],
              "optional": false
            }
          ]
        }
      ]
    }

  expect(gradesTotal(trackTestsValues)).toBe(55);
  });
})
