before(() => {
  cy.visit('/');
  cy.waitForReact(1000, '#root'); // 1000 is the timeout in milliseconds, you can provide as per AUT
});

describe('when I do not enter a grade', () => {
  it('has the number of grades set to 0', () => {
    let expectedNumberOfGradesEntered = 0;
    let regex = new RegExp(`^${expectedNumberOfGradesEntered}/[0-9]+`);

    cy.react('OverallResultDiv', { props: { resultLabel: 'Nombre de notes saisies' } })
      .find('span')
      .contains(regex);
  })

  it('has the overall average set to "-"', () => {
    const expectedOverallAverage = "-";
    const regex = new RegExp(`${expectedOverallAverage}/(.)+`);
    cy.react('OverallResultDiv', { props: { resultLabel: 'Votre moyenne' } })
      .find('span')
      .should(($div) => {
        const text = $div.text()
        expect(text).to.match(regex)
      })
  })

  it('has the merit set to "-"', () => {
    const expectedMerit = '-';
    const regex = new RegExp(`${expectedMerit}/(.)+`);
    cy.react('OverallResultDiv', { props: { resultLabel: 'Votre mention' } })
      .find('span')
      .should('have.text', expectedMerit)
  })
});

describe('when I enter a grade', () => {
  beforeEach(() => {
    cy.react('TestResultDiv', { props: { typeIndex: 0, testIndex: 1, resultIndex: 0 } })
      .type('10')
  })

  afterEach(() => {
    cy.react('TestResultDiv', { props: { typeIndex: 0, testIndex: 1, resultIndex: 0 } })
      .find('[type="number"]')
      .clear()
  });

  it('increments the number of grades of 1', () => {
    const expectedNumberOfGradesEntered = 1;
    const regex = new RegExp(`^${expectedNumberOfGradesEntered}/[0-9]+`);
    cy.react('OverallResultDiv', { props: { resultLabel: 'Nombre de notes saisies' } })
      .find('span')
      .contains(regex);
  })

  it('changes the overall average', () => {
    const expectedOverallAverage = 10;
    const regex = new RegExp(`${expectedOverallAverage}/(.)+`);
    cy.react('OverallResultDiv', { props: { resultLabel: 'Votre moyenne' } })
      .find('span')
      .should(($div) => {
        const text = $div.text()
        expect(text).to.match(regex)
      })
  })

  it('updates the merit', () => {
    const expectedMerit = 'Admis';
    const regex = new RegExp(`${expectedMerit}/(.)+`);
    cy.react('OverallResultDiv', { props: { resultLabel: 'Votre mention' } })
      .find('span')
      .should('have.text', expectedMerit)
  })

  describe('I reload the page', () => {
    it('keeps the number of grades updated', () => {
      cy.reload()
      cy.waitForReact(1000, '#root'); // 1000 is the timeout in milliseconds, you can provide as per AUT

      const expectedNumberOfGradesEntered = 1;
      const regex = new RegExp(`^${expectedNumberOfGradesEntered}/[0-9]+`);
      cy.react('OverallResultDiv', { props: { resultLabel: 'Nombre de notes saisies' } })
        .find('span')
        .contains(regex);
    })
  })

  describe('I switch to the other track', () => {
    it('resets the number of grades', () => {
      cy.react('ButtonTrack', { props: { trackIndex: 1 } })
        .click()

      const expectedNumberOfGradesEntered = 0;
      const regex = new RegExp(`^${expectedNumberOfGradesEntered}/[0-9]+`);
      cy.react('OverallResultDiv', { props: { resultLabel: 'Nombre de notes saisies' } })
        .find('span')
        .contains(regex);
    })
  })

  describe('I switch to the other track and then switch back to the original track', () => {
    it('displays the incremented number of grades again', () => {
      cy.react('ButtonTrack', { props: { trackIndex: 1 } })
        .click()
      cy.react('ButtonTrack', { props: { trackIndex: 0 } })
        .click()

    const expectedNumberOfGradesEntered = 1;
    const regex = new RegExp(`^${expectedNumberOfGradesEntered}/[0-9]+`);
    cy.react('OverallResultDiv', { props: { resultLabel: 'Nombre de notes saisies' } })
      .find('span')
      .contains(regex);
    })
  })

  describe('I click on the reset values button', () => {
    it('resets the number of grades', () => {
      cy.react('ReinitializeTestsValues')
        .click()

      const expectedNumberOfGradesEntered = 0;
      const regex = new RegExp(`^${expectedNumberOfGradesEntered}/[0-9]+`);
      cy.react('OverallResultDiv', { props: { resultLabel: 'Nombre de notes saisies' } })
        .find('span')
        .contains(regex);
    })
  })

  describe('I switch to the other theme', () => {
    it('changes the theme', () => {
      cy.get('html').then(($html) => {
        if ($html.hasClass('light')) {
          cy.react('ThemeToggle').click()
          cy.get('html').should('have.class', 'dark');
        } else {
          cy.react('ThemeToggle').click()
          cy.get('html').should('have.class', 'light');
        }
      })
    })

    it('keeps the number of grades unchanged', () => {
      cy.react('ThemeToggle').click()

      const expectedNumberOfGradesEntered = 1;
      const regex = new RegExp(`^${expectedNumberOfGradesEntered}/[0-9]+`);
      cy.react('OverallResultDiv', { props: { resultLabel: 'Nombre de notes saisies' } })
        .find('span')
        .contains(regex);
    })
  })
})
